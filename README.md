# FOXTECH MAP02 CONFIGURATION

![](IMAGES/02.jpg)


FOXTECH MAP02 - Configuration, mounting, and  wiring for Tarot 680 pro and pixhawk cube flight controller.
### [FOXTECH MAP02](https://www.foxtechfpv.com/foxtech-map-01-camera.html)
Foxtech Map-02 is a lite version mapping camera with APS-C Exmor CMOS sensor and **24.3 MP**. It is a good option for higher-quality mapping and survey.
It is a very light and small mapping camera, which is only **112.5g**. It is a good option for those customers who are looking for a small and light high-quality mapping camera. With the light weight and small size, Foxtech Map-02 camera can be easily integrated into UAVs and airplanes to do mapping jobs. Shutter trigger of Map-02 camera is relay trigger, and also it could be connected with PPK system to ensure a centimeter-level accuracy of mapping and survey through a Hot Shoe signal. And it is an interchangeable-lens camera, so you can change the camera's lens according to your needs.


### Wiring
 
 Foxtech Map2 camera support 5V relay trigger message for triggering with onboard flight controller. And we need 8.4V DC power supply for powering the camera. For this use a [Adjustable Buck Module](https://robu.in/product/mini-mp1584-dc-dc-adjustable-buck-module-3a/).
 ![buck module](IMAGES/index.jpeg)


 Foxtech Map2 support -ve trigger pulses to controll camera shutter. For setting camera trigger messages we need to wireup the camera with flight controller( we are using pixhawk cube)
 ![](IMAGES/map02-cale.jpg)

 ![](IMAGES/map02-cal.jpg)



 Pixhawk flight controller has an option to generate camera trigger messages and  a dedicated auxiliary channel(**AUX5**) exist for camera connection. So you can connect the camera trigger connection to the pixhawk directly.


 i.e., Connect Foxtech **YELLOW** to **AUX5 SIG** and **GREEN** to **AUX5 GND**.

 After wiring, connect the Pixhawk to the Mission Planner and set the following parameters according to the pictures below.

 ![](IMAGES/Screenshot_from_2019-11-18_18-33-49.png)

 ![](IMAGES/Screenshot_from_2019-11-18_18-47-34.png)



### Gimbal Setup 

![](IMAGES/photo6244455778199120269.jpg) 


**Components required:** [3D Printed parts](https://gitlab.com/icfoss/drone/foxtech-map02-configuration/tree/master/3D%20MODELS), ![3D](3D MODELS/Screenshot_from_2019-11-18_16-36-01.png)



[EMAX Servo](https://emaxmodel.com/es3352.html),![](IMAGES/servo.jpeg)  [5V 2A UBEC](https://www.banggood.in/2-in-1-5V12V-3A-UBEC-Voltage-Stabilizer-Step-Down-Module-for-2-6S-Lipo-Battery-p-1417148.html?rmmds=myorder&cur_warehouse=CN),![](IMAGES/ubec.jpeg)[Damping balls](https://robu.in/product/gimbal-vibration-damper-rubber-balls-four-pieces/), ![](IMAGES/gim.jpeg)



Print all the parts listed and assemble them using the servo and camera according to the instructions. The models listed are designed to be mount into the tarot frame.
We can setup the gimbal stebilisation and controll using Pixhawk and radio reciever. For this, connect servo to **MAIN OUT 7** of Pixhawk, connect 5V UBEC to any of the free AUX PORT(to provide enogh current to drive servo motor), and then connect pixhawk to computer and set following parameters on Missionplanner 



![](IMAGES/gimbal.jpeg)







